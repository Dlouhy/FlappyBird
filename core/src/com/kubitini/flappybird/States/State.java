package com.kubitini.flappybird.States;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector3;

/**
 * Abstract class for all states
 * Created by Kubitini on 12. 10. 2017.
 */

abstract class State {
    OrthographicCamera camera;
    GameStateManager gsm;

    State(GameStateManager gsm) {
        this.gsm = gsm;
        this.camera = new OrthographicCamera();
    }

    protected abstract void handleInput();
    public abstract void update(float dt);
    public abstract void render(SpriteBatch sb);
    public abstract void dispose();
}
