package com.kubitini.flappybird.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.kubitini.flappybird.FlappyBird;

/**
 * Game over state
 * Created by Kubitini on 7. 12. 2017.
 */

class GameOverState extends State {
    private Texture background;
    private Texture playBtn;
    private Texture gameOverText;

    private Music backgroundMusic;

    GameOverState(GameStateManager gsm) {
        super(gsm);
        camera.setToOrtho(false, FlappyBird.WIDTH / 2, FlappyBird.HEIGHT / 2);
        background = new Texture("bg.png");
        playBtn = new Texture("playbtn.png");
        gameOverText = new Texture("gameover.png");

        backgroundMusic = Gdx.audio.newMusic(Gdx.files.internal("gameover.mp3"));
        backgroundMusic.setLooping(true);
        backgroundMusic.setVolume(0.5f);
        backgroundMusic.play();
    }

    @Override
    public void handleInput() {
        if(Gdx.input.justTouched() || Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            gsm.set(new PlayState(gsm));
        }
    }

    @Override
    public void update(float dt) {
        handleInput();
    }

    @Override
    public void render(SpriteBatch sb) {
        sb.setProjectionMatrix(camera.combined);
        sb.begin();
        sb.draw(background, 0, 0);
        sb.draw(gameOverText, (camera.position.x - gameOverText.getWidth() / 2), camera.position.y + playBtn.getHeight());
        sb.draw(playBtn, (camera.position.x - playBtn.getWidth() / 2), camera.position.y - playBtn.getHeight() / 2);
        sb.end();
    }

    @Override
    public void dispose() {
        background.dispose();
        playBtn.dispose();
        backgroundMusic.dispose();
    }
}
