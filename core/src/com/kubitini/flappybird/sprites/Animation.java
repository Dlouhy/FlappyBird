package com.kubitini.flappybird.sprites;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

/**
 * Class for animating
 * Created by Kubitini on 17. 10. 2017.
 */

class Animation {
    private Array<TextureRegion> frames;
    private float maxFrameTime;
    private float currentFrameTime;
    private int frameCount;
    private int frame;
    private boolean isJumping;

    Animation(TextureRegion region, int frameCount, float cycleTime) {
        frames = new Array<TextureRegion>();
        int frameWidth = region.getRegionWidth() / frameCount;

        for(int i = 0; i < frameCount; i++) {
            frames.add(new TextureRegion(region, i * frameWidth, 0, frameWidth, region.getRegionHeight()));
        }

        this.frameCount = frameCount;
        maxFrameTime = cycleTime / frameCount;
        frame = 0;
        isJumping = false;
    }

    void update(float dt) {
        if(isJumping) {
            currentFrameTime += dt;

            if(currentFrameTime > maxFrameTime) {
                frame++;
                currentFrameTime = 0;
            }

            if(frame >= frameCount) {
                frame = 0;
                isJumping = false;
            }
        }
    }

    void jump() {
        isJumping = true;
    }

    TextureRegion getFrame() {
        return frames.get(frame);
    }
}
