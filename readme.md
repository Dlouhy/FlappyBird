2D Plošinovka
=============

Jedná se o jednoduchou hru na způsob známé hry Flappy Bird. Hra je vyvíjena pomocí knihovny [libGDX](https://libgdx.badlogicgames.com/), 
která je multiplatformní a open source.

Hra bude poskytovat možnost podívat se za jaký čas uživatel zvládl úroveň. Dále bude možné porovnávat své výsledky s ostatními hráči, protože se výsledky budou online ukládat na server, kde budou veřejně přístupné všem hráčům.